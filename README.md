注意
---
gitosc上停止更新，转移到github：[https://github.com/uikoo9/dishi](https://github.com/uikoo9/dishi)


[滴石](http://uikoo9.com/dishi)
==========================================
1. 取水滴石穿之意
2. 计划类app，一切始于计划成于计划

技术架构
---
1. html5+:[http://www.dcloud.io/docs/api/](http://www.dcloud.io/docs/api/)
2. mui:[http://dcloudio.github.io/mui/](http://dcloudio.github.io/mui/)
3. hbuilder:[http://www.dcloud.io/](http://www.dcloud.io/)

相关
---
1. osc代码开源：[http://git.oschina.net/uikoo9/uikoo9-dishi](http://git.oschina.net/uikoo9/uikoo9-dishi)
2. osc软件项目：[http://www.oschina.net/p/dishi](http://www.oschina.net/p/dishi)
3. 官网：[http://uikoo9.com/dishi](http://uikoo9.com/dishi)

捐助
---
希望得到您的捐助：

（支付宝捐助）

![zhifubao](http://uikoo9.qiniudn.com/@/img/donate/zhifu2.png)

（微信捐助）

![weixin](http://uikoo9.qiniudn.com/@/img/donate/zhifu1.png)